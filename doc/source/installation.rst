============
Installation
============

At the command line::

    $ pip install networking-hpe

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv networking-hpe
    $ pip install networking-hpe
