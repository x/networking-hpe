# Copyright (c) 2017 SuSE Linux Products GmbH
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
#

"""bm network provisioning
Revision ID: 9f1c0061fd83
Revises: start_networking_hpe
Create Date: 2017-07-20 15:03:36.870102
"""

# revision identifiers, used by Alembic.
revision = '9f1c0061fd83'
down_revision = 'start_networking_hpe'


def upgrade():
    pass
